<div class="row">
    <div class="col-md-12">
        <h3>Employees</h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Phone</th>
                <th scope="col">Salary</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;
            foreach ($rows as $row) {
                echo '<tr>';
                    echo '<th scope="row">'.$i++.'</th>';
                    echo '<td><a href="/index.php?a=m&p=i&action=read&id='.$row['id'].'">' . $row['name'] . '</a></td>';
                    echo '<td>' . $row['phone'] . '</td>';
                    echo '<td>' . $row['salary'] . '</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
