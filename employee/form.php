<?php
$name = '';
$phone = '';
$salary = '';
$action = 'create';
if (! empty($record) ) {
    $record = $record[0];
    $id = $record['id'];
    $name = $record['name'];
    $phone = $record['phone'];
    $salary = $record['salary'];
    if($record['id'] > 0) {
        $action = 'update';
    }
}
?>

<div class="row">

    <div class="col-md-12">

        <div class="jumbotron">
            <h1><a href="/index.php">Enter employee data</a></h1>
            <form id="emp-form" name="employeeform" action="/index.php?a=m&p=i&action=<?=$action?>&id=<?=$id?>" method="post">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Enter name" value="<?=$name?>">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="text" class="form-control" name="phone" id="phone" aria-describedby="phoneHelp" placeholder="Enter phone" value="<?=$phone?>">
                </div>
                <div class="form-group">
                    <label for="salary">Hourly Salary</label>
                    <input type="text" class="form-control" name="salary" id="salary" aria-describedby="salaryHelp" placeholder="Enter salary" value="<?=$salary?>">
                </div>
                <?php if($action==='update') : ?>
                    <input type="hidden" name="update" value="<?=$record['id']?>">
                    <input type="submit" class="btn btn-info" value="Update">
                    <a href="/index.php?a=m&p=i&action=delete&id=<?=$record['id']?>" class="btn btn-danger">Delete</a>
                <?php else: ?>
                    <input type="submit" class="btn btn-primary" value="Submit">
                <?php endif; ?>
            </form>
        </div>

    </div>
</div>

