<?php

class clsEmployee {
	public $name;
	public $salary;
	public $phone;

	private $validation;

	public function ShowHeader()
    {
        require_once './common/html_header.php';
    }

    public function ShowFooter()
    {
        require_once './common/html_footer.php';
    }

	public function ShowForm($record)
	{
		require_once './employee/form.php';
	}

	public function ShowTable()
    {
        $rows = Database::connect()->fetchAssoc('SELECT * FROM employee');
        require_once './employee/table.php';

    }

    public function ShowErrors($errors)
    {
        require_once './common/errors.php';
    }

	public function SaveToDb()
	{
	    if( ! $this->validate()) {
	        return $this->validation;
        }
	    $query = sprintf("INSERT INTO employee (name, phone, salary) values(:name, :phone, :salary)");
	    $params = [':name'=>$this->name, ':phone'=>$this->phone, ':salary'=>$this->salary];
	    Database::connect()->insert($query, $params);
	    return true;
	}

	public function Delete($id)
	{
        $query = "DELETE FROM employee WHERE id = ?";
        Database::connect()->delete($query, [$id]);
	}

	public function Update($id)
	{
        if( ! $this->validate()) {
            return $this->validation;
        }
        $query = sprintf("UPDATE employee SET name=:name, phone=:phone, salary=:salary WHERE id=:id");
        $params = [':id'=>$id, ':name'=>$this->name, ':salary'=>$this->salary, ':phone'=>$this->phone];
        Database::connect()->update($query, $params);
	}

    public function Read($id)
    {
        $query = "SELECT * FROM employee WHERE id = :id";
        $rows = Database::connect()->select($query, [':id'=>$id]);
        return $rows;
    }

    private function validate()
    {
        $validation = [];
        // Name
        if( empty($this->name) ) {
            $validation['name'][] = 'Name cannot be empty';
        }

        if(! is_string($this->name) || is_numeric($this->name)) {
            $validation['name'][] = 'Name must be string';
        }

        if(strlen($this->name) > 60) {
            $validation['name'][] = 'Name cannot be longer than 60 characters';
        }

        // Phone
        if( empty($this->phone) ) {
            $validation['phone'][] = 'Phone cannot be empty';
        }

        if(preg_match('/[^\d+-\s]/', $this->phone)) {
            $validation['phone'][] = 'Phone can only have numbers, + and -';
        }

        if(strlen($this->phone) > 19) {
            $validation['phone'][] = 'Phone cannot be longer than 20 characters';
        }

        // Salary
        if( empty($this->salary) ) {
            $validation['salary'][] = 'Salary cannot be empty';
        }

        if(! is_numeric($this->salary)) {
            $validation['salary'][] = 'Salary must be numeric';
        } elseif ($this->salary < 0) {
            $validation['salary'][] = 'Salary cannot be less than 0';
        }

        if(strlen($this->salary) > 17) {
            $validation['salary'][] = 'Salary cannot be longer than 8 characters';
        }

        $this->validation = $validation;
        return ! count($validation);
    }
}
