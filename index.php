<?php
session_start();
header('Expires: 10');
$include_check = 1;

define('DB_HOST', '127.0.0.1');
define('DB_UID', 'root');
define('DB_PSW', 'root');
define('DB_NAME', 'tj');
define('DB_NAME_ACC', 'root');

require_once('./common/Database.php');

// ============================================================

$area 	= isset($_GET['a']) ? $_GET['a'] : null;
$page 	= isset($_GET['p']) ? $_GET['p'] : null;
$show 	= isset($_GET['s']) ? $_GET['s'] : null;
$action = isset($_GET['action']) ? $_GET['action'] : null;

// Set default
if( ! in_array($area, ['m'])) {
    $area = 'm';
}

switch ($area)
{
	case 'm':
        require_once('common/area_main.php');
	    break;
}

