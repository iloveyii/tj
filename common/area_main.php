<?php
/***********************************/
if (!isset($include_check)) { die('script not meant to be called directly'); }

/***********************************/
$errors = [];
$record = [];
$id = 0;

if( ! in_array($page, ['i', 'h', 's'])) {
    $page = 'i';
}
if( $page == 'i' )
{
    require_once('employee/clsEmployee.php');
    $index = new clsEmployee($FkAccountId, $FkCompanyId);

    if ( $action == 'create' && $_SERVER['REQUEST_METHOD'] === 'POST' )
    {
        $index->name = $_POST['name'];
        $index->phone = $_POST['phone'];
        $index->salary = $_POST['salary'];
        $return = $index->SaveToDb();
        if($return !== true) {
            $errors = $return;
            $record[0] = [
                'name' => $_POST['name'],
                'phone' => $_POST['phone'],
                'salary' => $_POST['salary']
            ];
        }
    }

    if ( $action == 'delete')
    {
        $id=$_GET['id'];
        $index->Delete($id);
    }

    if ( $action == 'update' && $_SERVER['REQUEST_METHOD'] === 'POST')
    {
        $id=$_GET['id'];
        $index->name = $_POST['name'];
        $index->phone = $_POST['phone'];
        $index->salary = $_POST['salary'];
        $return = $index->Update($id);

        if($return !== true) {
            $errors = $return;
            $record[0] = [
                'name' => $_POST['name'],
                'phone' => $_POST['phone'],
                'salary' => $_POST['salary']
            ];
        }
    }


    if ( $action == 'read')
    {
        $id=$_GET['id'];
        $record = $index->Read($id);
    }

    $index->ShowHeader();
    $index->ShowErrors($errors);
    $index->ShowForm($record);
    $index->ShowTable();
    $index->ShowFooter();

}

if( $page == 'h' )
{
    require_once('hours/clsHour.php');
    $hour = new clsHour();

    if ( $action == 'create'  && $_SERVER['REQUEST_METHOD'] === 'POST')
    {
        $hour->employee_id = $_POST['employee_id'];
        $hour->dated = $_POST['dated'];
        $hour->checkin = $_POST['checkin'];
        $hour->checkout = $_POST['checkout'];
        $return = $hour->SaveToDb();

        if($return !== true) {
            $errors = $return;
            $record = [
                'employee_id'=>$_POST['employee_id'],
                'dated' => $_POST['dated'],
                'checkin' => $_POST['checkin'],
                'checkout' => $_POST['checkout']
            ];
        }
    }

    if ( $action == 'delete')
    {
        $id=$_GET['id'];
        $hour->Delete($id);
    }

    if ( $action == 'update' && $_SERVER['REQUEST_METHOD'] === 'POST')
    {
        $id=$_GET['id'];
        $hour->id = $_GET['id'];
        $hour->employee_id = $_POST['employee_id'];
        $hour->dated = $_POST['dated'];
        $hour->checkin = $_POST['checkin'];
        $hour->checkout = $_POST['checkout'];
        $return = $hour->Update($id);

        if($return !== true) {
            $errors = $return;
            $record = [
                'employee_id'=>$_POST['employee_id'],
                'dated' => $_POST['dated'],
                'checkin' => $_POST['checkin'],
                'checkout' => $_POST['checkout']
            ];
        }
    }


    if ( $action == 'read')
    {
        $id=$_GET['id'];
        $dated=$_GET['dated'];
        $record = $hour->Read($id, $dated);
    }

    $hour->ShowHeader();
    $hour->ShowErrors($errors);
    $hour->ShowForm($record);
    $hour->ShowTable();
    $hour->ShowFooter();

}

if( $page == 's' )
{
    require_once('salary/clsSalary.php');
    $salary = new clsSalary();

    if ( $action == 'create'  && $_SERVER['REQUEST_METHOD'] === 'POST')
    {
        $salary->employee_id = $_POST['employee_id'];
        $salary->dated = $_POST['dated'];
        $salary->checkin = $_POST['checkin'];
        $salary->checkout = $_POST['checkout'];
        $return = $salary->SaveToDb();

        if($return !== true) {
            $errors = $return;
            $record = [
                'employee_id'=>$_POST['employee_id'],
                'dated' => $_POST['dated'],
                'checkin' => $_POST['checkin'],
                'checkout' => $_POST['checkout']
            ];
        }
    }

    if ( $action == 'delete')
    {
        $id=$_GET['id'];
        $salary->Delete($id);
    }

    if ( $action == 'update' && $_SERVER['REQUEST_METHOD'] === 'POST')
    {
        $id=$_GET['id'];
        $salary->id = $_GET['id'];
        $salary->employee_id = $_POST['employee_id'];
        $salary->dated = $_POST['dated'];
        $salary->checkin = $_POST['checkin'];
        $salary->checkout = $_POST['checkout'];
        $return = $salary->Update($id);

        if($return !== true) {
            $errors = $return;
            $record = [
                'employee_id'=>$_POST['employee_id'],
                'dated' => $_POST['dated'],
                'checkin' => $_POST['checkin'],
                'checkout' => $_POST['checkout']
            ];
        }
    }


    if ( $action == 'read')
    {
        $id=$_GET['id'];
        $record = $salary->Read($id);
    }

    $salary->ShowHeader();
    $salary->ShowErrors($errors);
    echo '<div class="row">';
    $salary->ShowForm($record);
    $salary->ShowTable($id);
    echo '</div>';
    $salary->ShowFooter();

}
