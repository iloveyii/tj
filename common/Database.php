<?php

class Database {

    private static $instance;
    public $db;
    public $numRows;
    public $lastId;

    public static function connect()
    {
        if( isset(self::$instance) ) {
            return self::$instance;
        }

        $db = null;

        try
        {
            $connectionString =  sprintf("mysql:host=%s;dbname=%s;charset=utf8;", DB_HOST, DB_NAME);
            $db = new PDO($connectionString, DB_UID, DB_PSW);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (exception $e)
        {
            throw new Exception($e->getMessage());
        }

        self::$instance = new self();
        self::$instance->db = $db;

        return self::$instance;
    }

    public static function getInstance()
    {
        return self::$instance;
    }

    public function executeQuery($query)
    {
        self::connect();
        $sth = $this->db->prepare($query);
        $sth->execute();
    }

    public function select($query, $params)
    {
        $sth = $this->db->prepare($query);
        $sth->execute($params);
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        $this->lastId = $this->db->lastInsertId();
        $this->numRows = count($result) - 1;
        return $result;
    }

    public function selectOne($query, $params)
    {
        $sth = $this->db->prepare($query);
        $sth->execute($params);
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        $this->lastId = $this->db->lastInsertId();
        $this->numRows = count( (array)$result);
        return $result;
    }

    public function delete($query, $params)
    {
        $stmt = $this->db->prepare($query);
        $stmt->execute($params);
    }

    public function update($query, $params)
    {
        $stmt = $this->db->prepare($query);
        $num = $stmt->execute($params);
        $this->numRows = $num;
        return $num;
    }

    public function insert($query, $params=[])
    {
        self::connect();

        try
        {
            $sth = $this->db->prepare($query);
            $sth->execute($params);
            $this->lastId = $this->db->lastInsertId();
            return $this->lastId;
        }
        catch(PDOException $e)
        {
            throw new Exception($e->getMessage() + $query);
        }
        catch( Exception $e)
        {
            throw new Exception($e->getMessage() + $query);
        }
    }

    function fetchAssoc($query)
    {
        self::connect();
        try
        {
            $sth = $this->db->prepare($query);
            $sth->execute();
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
            $this->lastId = $this->db->lastInsertId();
            $this->numRows = count($result) - 1;
            return $result;
        }
        catch (PDOException $e)
        {
            throw new Exception($e->getMessage() . $query);
        }
        catch( Exception $e)
        {
            throw new Exception($e->getMessage() . $query);
        }
    }
}

