<!DOCTYPE html>
<html>
<head>
    <title>TJ - Payroll</title>
    <link rel="stylesheet" href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" />
</head>

<body>
<?php
$i = ''; $h = ''; $s = '';
if(isset($_GET['p'])) {
    switch ($_GET['p']) {
        case 'i':
            $i = 'active';
            break;
        case 'h':
            $h = 'active';
            break;
        case 's':
            $s = 'active';
            break;
    }
}
?>
<nav style="border-radius: 0" class="navbar navbar-toggleable-md navbar-inverse bg-inverse mb-4">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">TimeJ</a>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?=$i?>">
                <a class="nav-link" href="/index.php?a=m&p=i">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item <?=$h?>">
                <a class="nav-link" href="/index.php?a=m&p=h">Hours</a>
            </li>
            <li class="nav-item <?=$s?>">
                <a class="nav-link" href="/index.php?a=m&p=s">Salary</a>
            </li>
        </ul>
        <form class="form-inline mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>

<div class="container">

</div>
<div class="container">
