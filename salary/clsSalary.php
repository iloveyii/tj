<?php

class clsSalary {
    public $id;
    public $employee_id;
	public $dated;
	public $checkin;
	public $checkout;

	private $validation;

	public function ShowHeader()
    {
        require_once './common/html_header.php';
    }

    public function ShowFooter()
    {
        require_once './common/html_footer.php';
    }

	public function ShowForm($record)
	{
        $employees = Database::connect()->fetchAssoc('SELECT * FROM employee');
		require_once './salary/list.php';
	}

	public function ShowTable($id)
    {
        $query = "SELECT * FROM hours INNER JOIN employee ON hours.employee_id=employee.id WHERE employee_id = :id";
        $employeeHours = Database::connect()->select($query, [':id'=>$id]);
        require_once './salary/table.php';
    }

    public function ShowHours()
    {
        $hours = Database::connect()->fetchAssoc('SELECT * FROM hours');
        require_once './table.php';
    }


    public function ShowErrors($errors)
    {
        require_once './common/errors.php';
    }

	public function SaveToDb()
	{
	    if( ! $this->validate()) {
	        return $this->validation;
        }
	    $query = sprintf("INSERT INTO hours (employee_id, dated, checkin, checkout) values(:employee_id, :dated, :checkin, :checkout)");
	    $params = [':employee_id'=>$this->employee_id, ':dated'=>$this->dated, ':checkin'=>$this->checkin, ':checkout'=>$this->checkout];
	    Database::connect()->insert($query, $params);
	    return true;
	}

	public function Delete($id)
	{
        $query = "DELETE FROM employee WHERE id = ?";
        Database::connect()->delete($query, [$id]);
	}

	public function Update($id)
	{
        if( ! $this->validate()) {
            return $this->validation;
        }
        $query = sprintf("UPDATE hours SET dated=:dated, checkin=:checkin, checkout=:checkout WHERE id=:id");
        $params = [':id'=>$this->id, ':dated'=>$this->dated, ':checkin'=>$this->checkin, ':checkout'=>$this->checkout];
        Database::connect()->update($query, $params);
	}

    public function Read($id)
    {
        $query = "SELECT * FROM hours INNER JOIN employee ON hours.employee_id=employee.id WHERE employee_id = :id";
        $employeeHours = Database::connect()->select($query, [':id'=>$id]);
        return $employeeHours;
    }

    private function validate()
    {
        $validation = [];
        // Date
        if( empty($this->dated) ) {
            $validation['dated'][] = 'Date cannot be empty';
        }

        if(! $this->validDate($this->dated) ) {
            $validation['dated'][] = 'Date must be valid (Y-m-d)';
        }

        // Check in
        if( empty($this->checkin) ) {
            $validation['checkin'][] = 'Check in cannot be empty';
        }

        if(empty($this->checkout)) {
            $validation['checkout'][] = 'Checkout cannot be empty';
        }

        $this->validation = $validation;
        return ! count($validation);
    }

    function validDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

}
