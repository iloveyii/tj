<?php
$name = '';
$phone = '';
$salary = '';
$action = 'create';

$hoursId = 0;
$dated = date('Y-m-d');
$checkin = '08:00';
$checkout = '16:00';


if (! empty($record)) {
    $record = $record[0];
    $employee_id = $record['id'];
    $name = $record['name'];
    $phone = $record['phone'];
    $salary = $record['salary'];

    $query = "SELECT * FROM hours WHERE employee_id=:employee_id AND dated=:dated";
    $params = [':employee_id'=>$employee_id, ':dated'=>date('Y-m-d')];
    $employeeHours = Database::connect()->selectOne($query, $params);

    if(!empty($employeeHours) > 0) {
        $hoursId = $employeeHours['id'];
        $dated = $employeeHours['dated'];
        $checkin = $employeeHours['checkin'];
        $checkout = $employeeHours['checkout'];
        $action = 'update';
    }
}

?>
    <div class="col-md-4">
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                Employees list
            </a>
            <?php foreach ($employees as $row) :?>
                <a href="/index.php?a=m&p=s&action=read&id=<?=$row['id']?>" class="list-group-item list-group-item-action"><?=$row['name']?></a>
            <?php endforeach;?>
        </div>
    </div>


