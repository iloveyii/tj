    <div class="col-md-8">
        <h1>Salary</h1>
        <h4><?= isset( $employeeHours[0]) ? $employeeHours[0]['name'] : null ?></h4>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Date</th>
                    <th scope="col">Check In</th>
                    <th scope="col">Check Out</th>
                    <th scope="col">Worked Hours</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $i = 1; $totalHours = 0; $salary = null;
            foreach ($employeeHours as $row) {
                $checkin = new DateTime($row['checkin']);
                $checkout = new DateTime($row['checkout']);
                $interval = $checkin->diff($checkout);
                $diffTime = $interval->format("%H:%I");

                $numHours = $interval->format("%H");
                $totalHours = $totalHours + $numHours;
                $salary = $row['salary'];
                echo '<tr>';
                echo '<th scope="row">'.$i++.'</th>';
                echo '<td>' . $row['dated'] . '</td>';
                echo '<td>' . $row['checkin'] . '</td>';
                echo '<td>' . $row['checkout'] . '</td>';
                echo '<td>' . $diffTime  .  '</td>';
                echo '</tr>';
            }
            ?>
            <tfoot>
                <tr>
                    <td></td>
                    <td><strong>Total hours</strong></td>
                    <td></td>
                    <td></td>
                    <td><strong><?=$totalHours?></strong></td>
                </tr>
                <tr>
                    <td></td>
                    <td><strong>Total salary</strong></td>
                    <td>@<?=$salary?></td>
                    <td></td>
                    <td><strong><?=$totalHours * $salary ?></strong></td>
                </tr>
            </tfoot>
            </tbody>
        </table>
    </div>
