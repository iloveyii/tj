<?php

class clsHour {
    public $id;
    public $employee_id;
	public $dated;
	public $checkin;
	public $checkout;

	private $validation;

	public function ShowHeader()
    {
        require_once './common/html_header.php';
    }

    public function ShowFooter()
    {
        require_once './common/html_footer.php';
    }

	public function ShowForm($record)
	{
        $employees = Database::connect()->fetchAssoc('SELECT * FROM employee');
		require_once './hours/form.php';
	}

	public function ShowTable()
    {
        $hours = Database::connect()->fetchAssoc('SELECT hours.id, employee_id, dated, checkin, checkout, name, 
                                              phone, salary FROM hours INNER JOIN employee ON hours.employee_id=employee.id
                                              ORDER BY employee_id
                                              ');
        require_once './hours/table.php';
    }

    public function ShowHours()
    {
        $hours = Database::connect()->fetchAssoc('SELECT * FROM hours');
        require_once './table.php';
    }


    public function ShowErrors($errors)
    {
        require_once './common/errors.php';
    }

	public function SaveToDb()
	{
	    if( ! $this->validate()) {
	        return $this->validation;
        }
	    $query = sprintf("INSERT INTO  hours (employee_id, dated, checkin, checkout) values(:employee_id, :dated, :checkin, :checkout)");
	    $query = "INSERT INTO  hours (employee_id, dated, checkin, checkout) values(:employee_id, :dated, :checkin, :checkout)
                                 ON DUPLICATE KEY UPDATE checkin = :checkin, checkout = :checkout";
	    $params = [':employee_id'=>$this->employee_id, ':dated'=>$this->dated, ':checkin'=>$this->checkin, ':checkout'=>$this->checkout];
	    Database::connect()->insert($query, $params);
	    return true;
	}

	public function Delete($id)
	{
        $query = "DELETE FROM hours WHERE id = ?";
        Database::connect()->delete($query, [$id]);
	}

	public function Update($id)
	{
        if( ! $this->validate()) {
            return $this->validation;
        }
        $query = sprintf("UPDATE  hours SET dated=:dated, checkin=:checkin, checkout=:checkout WHERE id=:id");
        $params = [':id'=>$this->id, ':dated'=>$this->dated, ':checkin'=>$this->checkin, ':checkout'=>$this->checkout];
        Database::connect()->update($query, $params);
	}

    public function Read($id, $dated)
    {
        $query = "SELECT * FROM  hours WHERE id = :id AND dated=:dated";
        $rows = Database::connect()->select($query, [':id'=>$id, ':dated'=>$dated]);
        return $rows;
    }

    private function validate()
    {
        $validation = [];
        // Employee_id
        if($this->employee_id == 0) {
            $validation['employee_id'][] = 'employee_id cannot be 0, Click employee name in the list.';
        }
        // Date
        if( empty($this->dated) ) {
            $validation['dated'][] = 'Date cannot be empty';
        }

        if(! $this->validDate($this->dated) ) {
            $validation['dated'][] = 'Date must be valid (Y-m-d)';
        }

        // Check in
        if( empty($this->checkin) ) {
            $validation['checkin'][] = 'Check in cannot be empty';
        }
        if(! $this->validTime($this->checkin)) {
            $validation['checkin'][] = 'Check in is not a valid time';
        }

        if(empty($this->checkout)) {
            $validation['checkout'][] = 'Checkout cannot be empty';
        }
        if(! $this->validTime($this->checkout)) {
            $validation['checkout'][] = 'Check out is not a valid time';
        }

        $this->validation = $validation;
        return ! (count($validation) > 0);
    }

    function validDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    function validTime($time)
    {
        return  preg_match('/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/', $time) || preg_match('/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9]):([0-5][0-9])$/', $time);
    }

}
