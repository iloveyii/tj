<div class="row">
    <div class="col-md-12">
        <h3>Hours</h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Date</th>
                <th scope="col">Check In</th>
                <th scope="col">Check Out</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;
            foreach ($hours as $row) {
                $href = sprintf("/index.php?a=m&p=h&action=read&id=%d&dated=%s&employee_id=%d", $row['id'], $row['dated'], $row['employee_id']);
                echo '<tr>';
                echo '<th scope="row">'.$i++.'</th>';
                echo "<td><a href='$href'>" . $row['name'] . '</a></td>';
                echo '<td>' . $row['dated'] . '</td>';
                echo '<td>' . $row['checkin'] . '</td>';
                echo '<td>' . $row['checkout'] . '</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
