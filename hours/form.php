<?php
$name = '';
$phone = '';
$salary = '';
$action = 'create';

$hoursId = 0;
$dated = date('Y-m-d');
$checkin = '08:00';
$checkout = '16:00';
$employeeHours = [];
$employee_id = $_GET['employee_id'];

if( isset($_GET['id']) ) {
    $id = $_GET['id'];

    $query = "SELECT hours.id, dated, checkin, checkout, name FROM hours INNER JOIN employee ON hours.employee_id = employee.id WHERE hours.id=:id";
    $params = [':id' => $id];
    $employeeHours = Database::connect()->selectOne($query, $params);
} elseif (isset($_GET['employee_id'])) {
    $query = "SELECT hours.id, dated, checkin, checkout, name FROM hours RIGHT JOIN employee ON hours.employee_id = employee.id WHERE employee.id=:employee_id";
    $params = [':employee_id' => $_GET['employee_id']];
    $employeeHours = Database::connect()->selectOne($query, $params);
    $name = $employeeHours['name'];
    $employeeHours = [];
}

if(!empty($employeeHours)) {
    $name = $employeeHours['name'];
    $hoursId = $employeeHours['id'];
    $dated = empty($employeeHours['dated']) ? $dated : $employeeHours['dated'];
    $checkin = $employeeHours['checkin'] ? $employeeHours['checkin'] : $checkin;
    $checkout = $employeeHours['checkout'] ? $employeeHours['checkout'] : $checkout;
    $action = empty($employeeHours['dated']) ? 'create' : 'update';
}


?>
<div class="jumbotron">
    <div class="row">
        <div class="col-md-4">
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action active">
                    Employees list
                </a>
                <?php foreach ($employees as $row) :?>
                    <a href="/index.php?a=m&p=h&action=read&employee_id=<?=$row['id']?>" class="list-group-item list-group-item-action"><?=$row['name']?></a>
                <?php endforeach;?>
            </div>
        </div>

        <div class="col-md-8">
            <h1><a href="/index.php">Register working hours</a></h1>
            <strong> <?= $name ?></strong>
            <form id="emp-form" name="employee-form" action="/index.php?a=m&p=h&action=<?=$action?>&id=<?=$hoursId?>&employee_id=<?=$employee_id?>" method="post">
                <div class="form-group">
                    <label for="hours">Date</label>
                    <input type="text" class="form-control" name="dated" id="dated" aria-describedby="nameHelp" placeholder="Enter date" value="<?=$dated?>">
                </div>
                <div class="form-group">
                    <label for="hours">Check In</label>
                    <input type="text" class="form-control" name="checkin" id="checkin" aria-describedby="nameHelp" placeholder="Enter check in time" value="<?=$checkin?>">
                </div>
                <div class="form-group">
                    <label for="hours">Check Out</label>
                    <input type="text" class="form-control" name="checkout" id="checkout" aria-describedby="nameHelp" placeholder="Enter check out time" value="<?=$checkout?>">
                </div>
                <input type="hidden" name="employee_id" value="<?=$employee_id?>">
                <?php if($action==='update') : ?>
                    <input type="hidden" name="id" value="<?=$hoursId?>">
                    <input type="submit" class="btn btn-info" value="Update">
                    <a href="/index.php?a=m&p=h&action=delete&id=<?=$id?>" class="btn btn-danger">Delete</a>
                <?php else: ?>
                    <input type="submit" class="btn btn-primary" value="Submit">
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>

